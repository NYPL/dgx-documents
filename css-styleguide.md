# CSS Coding Style Guide

# References

* [18f CSS Coding Styeguide](https://pages.18f.gov/frontend/css-coding-styleguide/)
* [Airbnb CSS / Sass Styleguide](https://github.com/airbnb/css)

# Naming

* HTML elements should be in lowercase.

**Bad**

    BODY {
      // ...
    }

**Good**

    body {
      // ...
    }
	
* Name things clearly according to function, not appearance.
* Use Camel Case for classes and ids. Use hyphens only for BEM names (see below). Avoid underscores

**Bad**

    .classname,
    .class-name,
    .class_name,
    .CLASSNAME,
    .className {
      // ...
    }

**Good**

    .commentForm {
      // ...
    }

* Name CSS components and modules with singular nouns.

**Good**

    .button {
      // ...
    }

* Name modifiers and state-based rules with adjectives.

**Good**

    .hovered {
      // ...
    }

* If your CSS has to interface with other CSS libraries, consider namespacing every class.

**Good**

    .nyplHomepage {
      // ...
    }

## BEM (block-element-modifier)

Classic BEM with double hyphens (see [BEM 101](https://css-tricks.com/bem-101/)
for instance) can lead to long class names that are also harder to read, debug,
and manipulate in an inspector. Instead use single hyphens to separate blocks and elements, and separate classes for modifiers.

**Bad**

    .navBar
    .navBar--link
    .navBar--link--clicked

**Good**

    .navBar
    .navBar-link
    .navBar-link clicked

## Other guidlines

* Don't use Ids for styles
* Don't nest more than 4 layers deep
* Use ```!important``` only as a last resort
* Avoid generic rules on elements that may be inadvertently inherited. For instance, given a navigation bar where each item has a image to represent each section:

**Bad**

    .navBar-item {
      img {
        // ...
      }
    }

**Better**

    .navBar-item {
      & > img {
        // ...
      }
    }

**Possibly Best**

    .navBar-item {
      .navSectionImage {
        // ...
      }
    }

